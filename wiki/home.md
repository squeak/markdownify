
Markdownify is a simple wrapper of [markdown-it](https://markdown-it.github.io/markdown-it/) and [highlight.js](https://highlightjs.org/).
It includes preset themes and options to display markdown texts with nice formatting and code highlighting.
