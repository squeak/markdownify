# Usage

To use the library, import it in your scripts and stylesheets with:

```javascript
const markdownify = require("markdownify");
```

```less
@import "markdownify/styles/light";
// or
@import "markdownify/styles/dark";
```

You can now use it in your page like this:

```javascript
var textAsHtml = markdownify("#title\n\nsome text\n**some bold text**");
// textAsHtml is now an html string that you can just parse in the DOM
```
