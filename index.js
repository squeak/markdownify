var hljs = require('highlight.js');
var markdownIt = require("markdown-it");
var md = markdownIt({

  // // enable html tags in source
  // html: false,

  // convert \n to <br>
  breaks: true,

  // // convert urls to links
  // linkify: true,

  // use highlight.js for code highlighting
  highlight: function (str, lang) {
    if (lang && hljs.getLanguage(lang)) {
      try {
        return '<pre class="hljs"><code>' +
               hljs.highlight(lang, str, true).value +
               '</code></pre>';
      } catch (__) {}
    }

    return '<pre class="hljs"><code>' + md.utils.escapeHtml(str) + '</code></pre>';
  },

});

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: render markdown string to html
  ARGUMENTS: ( !text <markdwownString> )
  RETURN: <htmlString>
*/
module.exports = function (text) {
  var renderedText = md.render(text || "");
  if (_.isString(renderedText) && renderedText !== "") return '<span class="markdownify">'+ renderedText +"</span>"
  else return "";
};
